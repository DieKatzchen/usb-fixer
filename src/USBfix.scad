difference() {
	cube(size=[4,2,1],center=false);
	translate([0,-.5,0])
		rotate(a=[0,-45,0])
			cube(size=[2,3,2],center=false);
	}
